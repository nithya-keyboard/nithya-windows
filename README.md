Poorna is a Malayalam Extended inscript keyboard which includes all Malayalam Unicode charecters.
this is a source file for windows version. to edit this you need [Microsoft keyboard Layout Creator 1.4](https://www.microsoft.com/en-us/download/confirmation.aspx?id=102134)

# install
Download the release file\
unzip\
Double click setup to install

# Use
The layout is a four layer one, that is a single key maps to a maximum of 4 characters. For example,

in case of 'd',\
'd' gives ്\
'Shift+d' gives അ\
'Right Alt+d' gives ഽ\
'Right Alt+Shift+d' gives ഁ

# Layout Image
![Image](poorna.png)